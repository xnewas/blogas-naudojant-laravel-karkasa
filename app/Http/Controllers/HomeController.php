<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showWelcome()
    {

        return view('home', array('posts' => Post::all()->sortByDesc('id')->take(10)));
    }  //
}
