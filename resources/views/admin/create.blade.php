
@extends('base')

@section('content')
    <h2>Naujas blogo įrašas</h2>

    @if($errors->any())
        @foreach ($errors->all() as $error)
            {{ $error }}<br />
        @endforeach
        <br />
    @endif

    {!! Form::open(array('url' => 'admin/create')) !!}
    Blogo title:
    <br />
    {!! Form::text('title', old('title')) !!}
    <br />Tekstas:
    <br />
    {!! Form::textarea('text', '') !!}
    <br />
    {!! Form::submit('Saugoti') !!}
    {!! Form::close() !!}
@stop