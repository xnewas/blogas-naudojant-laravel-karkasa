@extends('base')
@section('content')


@if ($errors->any())
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

{!! Form::open(['url' => 'register', 'class' => 'px-4 py-3']) !!}
<h2 class="text-center">Registracija</h2>
 <div class="form-group">
     <label for="exampleDropdownFormName1">Vardas</label>
{!! Form::text('name', old('name'), ['class' => 'form-control','id' => 'exampleDropdownFormName1', 'placeholder' => 'Vardas']) !!}
 </div>
<hr>
 <div class="form-group">
     <label for="exampleDropdownFormEmail1">El.pašto adresas</label>
<br />
{!! Form::email('email', old('email'), ['class' => 'form-control','id' => 'exampleDropdownFormEmail1', 'placeholder' => 'El.paštas']) !!}
 </div>
 <hr>
 <div class="form-group">
     <label for="exampleDropdownFormPass1">Slaptažodis</label>
{!! Form::password('password', ['class' => 'form-control','id' => 'exampleDropdownFormPass1', 'placeholder' => 'Slaptažodis']) !!}
 </div>
 <hr>
 <div class="form-group">
     <label for="exampleDropdownFormRepeatPass1">Pakartoti slaptažodį</label>
{!! Form::password('password_confirmation', ['class' => 'form-control','id' => 'exampleDropdownFormRepeatPass1', 'placeholder' => 'Pakartokite slaptažodį']) !!}
 </div>
 <hr>
<button type="submit"class="btn btn-primary">Registruotis</button>
{!! Form::close() !!}
 <div class="dropdown-divider"></div>
 <a class="dropdown-item text-center" href="{{url('/login')}}">Jau užsiregistravęs? Prisijunk !!!!</a>
    </div>
    @stop