<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ 'Tinkliaraštis' }}</title>
    <!-- Bootstrap Core CSS -->
    <link href=" {{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href=" {{ asset('css/clean-blog.min.css') }} ">
    <!-- Custom Fonts -->
    <link href=" {{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Pagrindinis puslapis</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('about') }}">Apie mane</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('kategorijos') }}">Įrašų kategorijos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('contact') }}">Kontaktai</a>
                </li>
                @if (Auth::check())
                    <li class="nav-item"><a class="nav-link" href="{{ url('logout') }}">Atsijungti</a></li>
                    @else
                    <li class="nav-item"><a class="nav-link" href="{{ url('login') }}">Prisijungti</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url('register') }}">Registruotis</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<!-- Page Header -->
<header class="masthead" style="background-image: url('{{ asset('/images/home-bg.jpg') }}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>Tinkliaraštis</h1>
                    <span class="subheading">Tinklo žinios :)))</span>
                </div>
            </div>
        </div>
    </div>
</header>

<main>
    @yield('content')
</main>
<!-- Footer -->
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <ul class="list-inline text-center">
                    <li class="list-inline-item">
                        <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
                        </a>
                    </li>
                </ul>
                <p class="copyright text-muted">2019 &copy; Kęstas Daukantas 2019, panaudotas nemokamas dizaino šablonas</p>
            </div>
        </div>
    </div>
</footer>

    <!-- jQuery -->
        <script src=" {{ asset('vendor/jquery/jquery.min.js') }}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src=" {{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>

        <!-- Contact Form JavaScript -->
        <script src=" {{ asset('js/jqBootstrapValidation.js') }}"></script>
        <script src=" {{ asset('js/contact_me.js') }}"></script>

        <!-- Theme JavaScript -->
        <script src=" {{ asset('js/clean-blog.min.js') }}"></script>
</body>
</html>