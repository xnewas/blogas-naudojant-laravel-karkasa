@extends('base')
@section('content')



{!! Form::open(['url' => 'login', 'class' => 'px-4 py-3']) !!}
<h2 class="text-center">Prisijungimas</h2>
                <div class="form-group">
                    <label for="exampleDropdownFormEmail1">El.pašto adresas</label>
{!! Form::email('email', old('email'), ['class' => 'form-control','id' => 'exampleDropdownFormEmail1', 'placeholder' => 'El.paštas']) !!}
                </div>
                    <hr>
            <div class="form-group">
                    <label for="exampleDropdownFormPass">Slaptažodis</label>
{!! Form::password('password', ['class' => 'form-control','id' => 'exampleDropdownFormPass', 'placeholder' => 'Slaptažodis']) !!}
            </div>
<hr>
<button type="submit"class="btn btn-primary">Prisijungti</button>
{!! Form::close() !!}
<div class="dropdown-divider"></div>
<a class="dropdown-item text-center" href="{{url('/register')}}">Dar neužsiregistravęs? Užsiregistruok !!!!</a>
<a class="dropdown-item text-center" href="#">Pamiršote slatažodį?</a>
</div>
@stop