@extends('base')
@section('content')
    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <h2 class="section-heading">{{ ($post->title) }}</h2>

                    <p>{{ ($post->text) }}</p>
                    <blockquote class="blockquote">{{ ($post->title) }}</blockquote>
                        {{--<img class="img-fluid" src="{{asset('images/post-sample-image.jpg')}}" alt="">--}}
                    {{--<span class="caption text-muted">hahaha.</span>--}}
                    <p class="post-meta">Įrašo autorius
                        <a href="#">{{ $post->author }}</a>
                        {{ $post->created_at }}</p>
                    <div class="clearfix">
                        <a class="btn btn-primary float-right" href="{{ url('/') }}">Grįžti į pagrindinį puslapį &rarr;</a>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <hr>

  @stop